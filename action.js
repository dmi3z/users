let users = [
    {
        id: 0,
        name: {
            first: 'Dmitry',
            last: 'Zhitnikov'
        },
        email: 'test@test.ru',
        age: 31
    },
    {
        id: 1,
        name: {
            first: 'Ivan',
            last: 'Ivanov'
        },
        email: 'ivan@test.ru',
        age: 27
    },
    {
        id: 2,
        name: {
            first: 'Sergey',
            last: 'Petrov'
        },
        email: 'sergey@test.ru',
        age: 25
    },
    {
        id: 3,
        name: {
            first: 'Kirill',
            last: 'Semenov'
        },
        email: 'kirill@test.ru',
        age: 15
    },
    {
        id: 4,
        name: {
            first: 'Nikolay',
            last: 'Nikolaev'
        },
        email: 'nikolay@test.ru',
        age: 37
    },
    {
        id: 5,
        name: {
            first: 'Alexey',
            last: 'Viktorov'
        },
        email: 'alexey@test.ru',
        age: 30
    }
];

let container = document.querySelector('.container');
document.querySelector('#asc').addEventListener('click', sortAsc);
document.querySelector('#desc').addEventListener('click', sortDesc);
document.querySelector('#save').addEventListener('click', addUser);

function createUserCard(user) {
    let card = `<div class="card">
                    <img class="card-img" src="./images/user.jpg" alt="">

                <div class="info">
                    <span class="user-name">${user.name.first} ${user.name.last}</span>
                    <span class="user-email">${user.email}</span>
                    <span class="user-age">${user.age}</span>
                </div>
            </div>`;
    return card;
}


function showUsers() {
    users.forEach((item) => {
        container.innerHTML += createUserCard(item);
    });
}

// showUsers();

// -----

function createCardTag(user) {
    let card = document.createElement('div');
    card.classList.add('card');

    let img = document.createElement('img');
    img.classList.add('card-img');
    img.src = './images/user.jpg';
    // img.setAttribute('src', './images/user.jpg');

    let info = document.createElement('div');
    info.classList.add('info');

    let name = document.createElement('span');
    name.classList.add('user-name');
    name.innerText = user.name.first + ' ' + user.name.last;

    let email = document.createElement('span');
    email.classList.add('user-email');
    email.innerText = user.email;

    let age = document.createElement('span');
    age.classList.add('user-age');
    age.innerText = user.age;

    let close = document.createElement('div');
    close.classList.add('close');
    close.innerText = 'X';
    close.addEventListener('click', () => deleteUser(user.id));

    info.appendChild(name);
    info.appendChild(email);
    info.appendChild(age);

    card.appendChild(img);
    card.appendChild(info);
    card.appendChild(close);
    return card;
}

function drawCards() {
    container.innerHTML = '';
    users.forEach(item => {
        let card = createCardTag(item);
        container.appendChild(card);
    });
}

drawCards();

function sortAsc() {
    users.sort((a, b) => a.age - b.age);
    drawCards();
}

function sortDesc() {
    users.sort((a, b) => b.age - a.age);
    drawCards();
}

function deleteUser(id) {
    /* let user = users.find(elem => elem.id === id);
    let index = users.indexOf(user);
    users.splice(index, 1); */
    users = users.filter(elem => elem.id !== id);
    drawCards();
}

function addUser() {
    let nameInput = document.querySelector('#name');
    let emailInput = document.querySelector('#email');
    let ageInput = document.querySelector('#age');

    // 'text 1 yesy jhf'.split(' ') => ['text', '1', 'yesy' ... ]
    // 'First Last'.split(' ') => ['First', 'Last']

    let newUser = {
        id: getRandomId(),
        name: {
            first: nameInput.value.split(' ')[0],
            last: nameInput.value.split(' ')[1]
        },
        email: emailInput.value,
        age: +ageInput.value
    };

    users.push(newUser);
    drawCards();

    nameInput.value = '';
    emailInput.value = '';
    ageInput.value = '';
}

function getRandomId() {
    return Math.floor(Math.random() * 900) + 100;
}
